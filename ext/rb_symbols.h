/*
 * StarRuby Ruby Symbols
 * WARNING This file was generated, do not modify, else all changes will be lost
 */

#ifndef STARRUBY_SYMBOLS_H_
#define STARRUBY_SYMBOLS_H_

#include <ruby.h>

 VALUE ID_add;
 VALUE ID_alpha;
 VALUE ID_angle;
 VALUE ID_array_get;
 VALUE ID_array_set;
 VALUE ID_background;
 VALUE ID_basename;
 VALUE ID_blend_type;
 VALUE ID_blur;
 VALUE ID_bold;
 VALUE ID_camera_height;
 VALUE ID_camera_pitch;
 VALUE ID_camera_roll;
 VALUE ID_camera_x;
 VALUE ID_camera_y;
 VALUE ID_camera_yaw;
 VALUE ID_center_x;
 VALUE ID_center_y;
 VALUE ID_clear;
 VALUE ID_color;
 VALUE ID_compact;
 VALUE ID_compact_bang;
 VALUE ID_compare;
 VALUE ID_cursor;
 VALUE ID_delay;
 VALUE ID_destroy;
 VALUE ID_device_number;
 VALUE ID_dispose;
 VALUE ID_divide;
 VALUE ID_down;
 VALUE ID_draw;
 VALUE ID_dst_mask;
 VALUE ID_duration;
 VALUE ID_expand_path;
 VALUE ID_extname;
 VALUE ID_file_question;
 VALUE ID_fps;
 VALUE ID_frame_rate;
 VALUE ID_fullscreen;
 VALUE ID_gamepad;
 VALUE ID_get_size;
 VALUE ID_glob;
 VALUE ID_hash;
 VALUE ID_height;
 VALUE ID_initialize;
 VALUE ID_inspect;
 VALUE ID_intersection_x;
 VALUE ID_intersection_y;
 VALUE ID_interval;
 VALUE ID_io_length;
 VALUE ID_italic;
 VALUE ID_keyboard;
 VALUE ID_left;
 VALUE ID_length;
 VALUE ID_loop;
 VALUE ID_mask;
 VALUE ID_matrix;
 VALUE ID_middle;
 VALUE ID_mouse;
 VALUE ID_multiply;
 VALUE ID_new;
 VALUE ID_none;
 VALUE ID_pack;
 VALUE ID_palette;
 VALUE ID_panning;
 VALUE ID_position;
 VALUE ID_post_render;
 VALUE ID_pre_render;
 VALUE ID_render;
 VALUE ID_right;
 VALUE ID_saturation;
 VALUE ID_scale;
 VALUE ID_scale_vec2;
 VALUE ID_scale_vec3;
 VALUE ID_scale_x;
 VALUE ID_scale_y;
 VALUE ID_size;
 VALUE ID_src_height;
 VALUE ID_src_mask;
 VALUE ID_src_rect;
 VALUE ID_src_width;
 VALUE ID_src_x;
 VALUE ID_src_y;
 VALUE ID_strip_bang;
 VALUE ID_sub;
 VALUE ID_subtract;
 VALUE ID_time;
 VALUE ID_title;
 VALUE ID_to_a;
 VALUE ID_to_f;
 VALUE ID_to_h;
 VALUE ID_to_i;
 VALUE ID_to_s;
 VALUE ID_tone;
 VALUE ID_ttc_index;
 VALUE ID_underline;
 VALUE ID_unpack;
 VALUE ID_up;
 VALUE ID_view_angle;
 VALUE ID_volume;
 VALUE ID_vsync;
 VALUE ID_width;
 VALUE ID_window_scale;
 VALUE ID_x;
 VALUE ID_y;
 VALUE symbol_add;
 VALUE symbol_alpha;
 VALUE symbol_angle;
 VALUE symbol_array_get;
 VALUE symbol_array_set;
 VALUE symbol_background;
 VALUE symbol_basename;
 VALUE symbol_blend_type;
 VALUE symbol_blur;
 VALUE symbol_bold;
 VALUE symbol_camera_height;
 VALUE symbol_camera_pitch;
 VALUE symbol_camera_roll;
 VALUE symbol_camera_x;
 VALUE symbol_camera_y;
 VALUE symbol_camera_yaw;
 VALUE symbol_center_x;
 VALUE symbol_center_y;
 VALUE symbol_clear;
 VALUE symbol_color;
 VALUE symbol_compact;
 VALUE symbol_compact_bang;
 VALUE symbol_compare;
 VALUE symbol_cursor;
 VALUE symbol_delay;
 VALUE symbol_destroy;
 VALUE symbol_device_number;
 VALUE symbol_dispose;
 VALUE symbol_divide;
 VALUE symbol_down;
 VALUE symbol_draw;
 VALUE symbol_dst_mask;
 VALUE symbol_duration;
 VALUE symbol_expand_path;
 VALUE symbol_extname;
 VALUE symbol_file_question;
 VALUE symbol_fps;
 VALUE symbol_frame_rate;
 VALUE symbol_fullscreen;
 VALUE symbol_gamepad;
 VALUE symbol_get_size;
 VALUE symbol_glob;
 VALUE symbol_hash;
 VALUE symbol_height;
 VALUE symbol_initialize;
 VALUE symbol_inspect;
 VALUE symbol_intersection_x;
 VALUE symbol_intersection_y;
 VALUE symbol_interval;
 VALUE symbol_io_length;
 VALUE symbol_italic;
 VALUE symbol_keyboard;
 VALUE symbol_left;
 VALUE symbol_length;
 VALUE symbol_loop;
 VALUE symbol_mask;
 VALUE symbol_matrix;
 VALUE symbol_middle;
 VALUE symbol_mouse;
 VALUE symbol_multiply;
 VALUE symbol_new;
 VALUE symbol_none;
 VALUE symbol_pack;
 VALUE symbol_palette;
 VALUE symbol_panning;
 VALUE symbol_position;
 VALUE symbol_post_render;
 VALUE symbol_pre_render;
 VALUE symbol_render;
 VALUE symbol_right;
 VALUE symbol_saturation;
 VALUE symbol_scale;
 VALUE symbol_scale_vec2;
 VALUE symbol_scale_vec3;
 VALUE symbol_scale_x;
 VALUE symbol_scale_y;
 VALUE symbol_size;
 VALUE symbol_src_height;
 VALUE symbol_src_mask;
 VALUE symbol_src_rect;
 VALUE symbol_src_width;
 VALUE symbol_src_x;
 VALUE symbol_src_y;
 VALUE symbol_strip_bang;
 VALUE symbol_sub;
 VALUE symbol_subtract;
 VALUE symbol_time;
 VALUE symbol_title;
 VALUE symbol_to_a;
 VALUE symbol_to_f;
 VALUE symbol_to_h;
 VALUE symbol_to_i;
 VALUE symbol_to_s;
 VALUE symbol_tone;
 VALUE symbol_ttc_index;
 VALUE symbol_underline;
 VALUE symbol_unpack;
 VALUE symbol_up;
 VALUE symbol_view_angle;
 VALUE symbol_volume;
 VALUE symbol_vsync;
 VALUE symbol_width;
 VALUE symbol_window_scale;
 VALUE symbol_x;
 VALUE symbol_y;

#endif
