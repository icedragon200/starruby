/*
  StarRuby Version [HEADER]
  vr 1.0.0
  */
#ifndef STARRUBY_VERSION_H
  #define STARRUBY_VERSION_H

  #define STRB_VERSION_MAJOR 0
  #define STRB_VERSION_MINOR 5
  #define STRB_VERSION_REV 4
  #define STRB_VERSION_S "0.4.5"

#endif
